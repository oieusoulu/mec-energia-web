import { isValidCnpj } from "../src/utils/cnpj";
import '@testing-library/jest-dom'


describe('Test CNPJ', () => {
  it("isValidCnpj retorna true para CNPJ válido", () => {
    const cnpj = "12.345.678/0001-95";
    expect(isValidCnpj(cnpj)).toBe(true);
  });
  it("isValidCnpj retorna mensagem para CNPJ pequeno", () => {
    const cnpj = "12.345.678";
    expect(isValidCnpj(cnpj)).toBe("Insira um CNPJ válido com 14 dígitos.");
  });
  it("isValidCnpj retorna mensagem para primeiro DV invalido", () => {
    const cnpj = "12.345.678/0001-15";
    expect(isValidCnpj(cnpj)).toBe("O primeiro dígito verificador do CNPJ é inválido.");
  });
  it("isValidCnpj retorna mensagem para segundo DV invalido", () => {
    const cnpj = "12.345.678/0001-91";
    expect(isValidCnpj(cnpj)).toBe("O segundo dígito verificador do CNPJ é inválido.");
  });
})

