export const isValidCnpj = (cnpj: string): string | true => {

  const cnpjRegex = /([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})/;
  if (!cnpjRegex.test(cnpj)) return 'Insira um CNPJ válido com 14 dígitos.';

  const cnpjNumerico = cnpj.replace(/[\/.-]/g, '');
  const digitos = cnpjNumerico.split('').map(Number);
  const mult = [5, 6, 7, 8, 9, 2, 3, 4, 5, 6, 7, 8, 9]

  let soma1 = 0;
  for (let i = 0; i < 12; i++) soma1 += digitos[i] * mult[i + 1];
  const resto1 = soma1 % 11;
  if (resto1 !== digitos[12]) return "O primeiro dígito verificador do CNPJ é inválido.";

  let soma2 = 0;
  for (let i = 0; i < 13; i++) soma2 += digitos[i] * mult[i];
  const resto2 = soma2 % 11;
  if (resto2 !== digitos[13]) return "O segundo dígito verificador do CNPJ é inválido.";

  return true;
};
